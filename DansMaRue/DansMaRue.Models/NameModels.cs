﻿using System;
using System.Collections.Generic;

namespace DansMaRue.Models
{
    public class Fields
    {
        public int nombre { get; set; }
        public string annee { get; set; }
        public string prenoms { get; set; }
        public string sexe { get; set; }
    }

    public class Record
    {
        public string datasetid { get; set; }
        public string recordid { get; set; }
        public Fields fields { get; set; }
        public DateTime record_timestamp { get; set; }
    }

    public class Facet
    {
        public string name { get; set; }
        public string path { get; set; }
        public int count { get; set; }
        public string state { get; set; }
    }

    public class FacetGroup
    {
        public string name { get; set; }
        public List<Facet> facets { get; set; }
    }

    public class ListNameModel
    {
        public int nhits { get; set; }
        public List<Record> records { get; set; }
        public List<FacetGroup> facet_groups { get; set; }
    }
}
