﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microcharts;
using SkiaSharp;
using Xamarin.Forms;

namespace DansMaRue
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();

            NavigationPage.SetHasNavigationBar(this, false);

            NameButton.GestureRecognizers.Add(new TapGestureRecognizer
            {
                Command = new Command(() =>
                {
                    // Navigate to new view
                    Navigation.InsertPageBefore(new SelectionNamePage(), this);
                    Navigation.PopAsync().ConfigureAwait(false);
                }),
                NumberOfTapsRequired = 1
            });
        }

        void Handle_ValueChanged(object sender, int e)
        {
            LoadGraph();
        }

        private void LoadGraph()
        {
            Random random = new Random();
            int january = random.Next(100, 800);
            int feb = random.Next(100, 800);
            int march = random.Next(100, 800);
            int aprl = random.Next(100, 800);

            var entries = new[]
            {
                new Microcharts.Entry(january)
                {
                    Label = "January",
                    ValueLabel = january.ToString(),
                    Color = SKColor.Parse("#FE535F")
                },
                new Microcharts.Entry(feb)
                {
                    Label = "February",
                    ValueLabel = feb.ToString(),
                    Color = SKColor.Parse("#FD4D48")
                },
                new Microcharts.Entry(march)
                {
                    Label = "March",
                    ValueLabel = march.ToString(),
                    Color = SKColor.Parse("#FE3049")
                }
                ,
                new Microcharts.Entry(aprl)
                {
                    Label = "April",
                    ValueLabel = aprl.ToString(),
                    Color = SKColor.Parse("#FCAA45")
                }
            };

            DataChartView.Chart = new DonutChart() 
            { 
                Entries = entries,
                LabelTextSize = 24
            };
        }
    }
}
