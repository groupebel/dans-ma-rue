﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DansMaRue.ViewModels;
using GalaSoft.MvvmLight.Messaging;
using Xamarin.Forms;

namespace DansMaRue
{
    public partial class SelectionNamePage : ContentPage
    {
        NameViewModel nameViewModel;

        public SelectionNamePage()
        {
            InitializeComponent();

            nameViewModel = new NameViewModel();

            NavigationPage.SetHasNavigationBar(this, false);

            NameButton.GestureRecognizers.Add(new TapGestureRecognizer
            {
                Command = new Command(() =>
                {
                    // Navigate to new view
                    Navigation.InsertPageBefore(new MainPage(), this);
                    Navigation.PopAsync().ConfigureAwait(false);
                }),
                NumberOfTapsRequired = 1
            });

            Messenger.Default.Register<RefreshCardMessage>(this, RefreshCard);

            nameViewModel.LoadNamesCommand.Execute(null);

            this.BindingContext = nameViewModel;
        }

        private void RefreshCard(RefreshCardMessage msg)
        {
            //NameCards.ItemsSource = nameViewModel.Names;
        }

        void Handle_Swiped(object sender, MLToolkit.Forms.SwipeCardView.Core.SwipedCardEventArgs e)
        {
        }

        void Handle_Dragging(object sender, MLToolkit.Forms.SwipeCardView.Core.DraggingCardEventArgs e)
        {

        }
    }
}
