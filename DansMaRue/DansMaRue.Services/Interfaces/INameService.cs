﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DansMaRue.Models;

namespace DansMaRue.Services.Interfaces
{
    public interface INameService
    {
        Task<ListNameModel> GetListNames();
    }
}
