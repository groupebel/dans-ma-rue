﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using DansMaRue.Models;
using DansMaRue.Services.Interfaces;
using Newtonsoft.Json;

namespace DansMaRue.Services
{
    public class NameService : INameService
    {
        private string URL = "https://opendata.paris.fr/api/records/1.0/search/?dataset=liste_des_prenoms_2004_a_2012&rows=500&sort=annee&facet=sexe&facet=annee&facet=prenoms";
        HttpClient Client;

        public NameService()
        {
            if (Client == null)
            {
                Client = new HttpClient();
            }
        }

        public async Task<ListNameModel> GetListNames()
        {
            var uri = new Uri(URL);
            var response = await Client.GetAsync(uri);

            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                var rootObject = JsonConvert.DeserializeObject<ListNameModel>(content);

                return rootObject;
            }

            return new ListNameModel();
        }
    }
}
