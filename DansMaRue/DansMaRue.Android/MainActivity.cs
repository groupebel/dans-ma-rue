﻿using System;

using Android.App;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using SegmentedControl.FormsPlugin.Android;
using GalaSoft.MvvmLight.Ioc;
using DansMaRue.Services.Interfaces;
using DansMaRue.Services;

namespace DansMaRue.Droid
{
    [Activity(Label = "DansMaRue", Icon = "@mipmap/icon", Theme = "@style/MainTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(savedInstanceState);
            global::Xamarin.Forms.Forms.Init(this, savedInstanceState);

            SegmentedControlRenderer.Init();

            // INIT SERVICES
            SimpleIoc.Default.Register<INameService, NameService>();

            LoadApplication(new App());
        }
    }
}