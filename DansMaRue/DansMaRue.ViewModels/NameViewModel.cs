﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using DansMaRue.Services.Interfaces;
using DansMaRue.ViewModels.Items;
using DoLess.Commands;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Ioc;
using GalaSoft.MvvmLight.Messaging;

namespace DansMaRue.ViewModels
{
    public class NameViewModel : ViewModelBase
    {
        INameService nameService;

        public ICommand LoadNamesCommand { get; set; }
        public ICommand SwipedCommand { get; set; }

        private ObservableCollection<NameItemViewModel> _names;
        public ObservableCollection<NameItemViewModel> Names
        {
            get => _names;
            set
            {
                _names = value;
                this.RaisePropertyChanged();
            }
        }

        public NameViewModel()
        {
            nameService = SimpleIoc.Default.GetInstance<INameService>();

            Names = new ObservableCollection<NameItemViewModel>();
            LoadNamesCommand = Command.CreateFromTask(LoadNames);
            SwipedCommand = Command.CreateFromAction(Swiped);
        }

        private async Task LoadNames()
        {
            Names.Clear();
            var list = await nameService.GetListNames();

            var listRandom = ShuffleList(list.records);

            foreach (var item in listRandom)
            {
                var nameItem = new NameItemViewModel(item.fields.prenoms, item.fields.sexe, item.fields.nombre.ToString());
                Names.Add(nameItem);
            }

            Messenger.Default.Send(new RefreshCardMessage());
        }

        private List<E> ShuffleList<E>(List<E> inputList)
        {
            List<E> randomList = new List<E>();

            Random r = new Random();
            int randomIndex = 0;
            while (inputList.Count > 0)
            {
                randomIndex = r.Next(0, inputList.Count); //Choose a random object in the list
                randomList.Add(inputList[randomIndex]); //add it to the new, random list
                inputList.RemoveAt(randomIndex); //remove to avoid duplicates
            }

            return randomList; //return the new random list
        }

        private void Swiped()
        {

        }
    }

    public class RefreshCardMessage
    {
        public RefreshCardMessage()
        {

        }
    }
}
