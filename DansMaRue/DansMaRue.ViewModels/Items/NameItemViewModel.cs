﻿using System;
namespace DansMaRue.ViewModels.Items
{
    public class NameItemViewModel
    {
        public string Name { get; set; }
        public string Type { get; set; }
        public string Count { get; set; }


        public NameItemViewModel(string name, string type, string count)
        {
            this.Name = name;
            this.Type = type;
            this.Count = "Donné " + count + " fois";
        }
    }
}
